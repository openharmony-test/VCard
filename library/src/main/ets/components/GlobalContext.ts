/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

export class GlobalContext {
  private constructor() {
  }
  public static parseInt(str, radix?:number):number{
    let str_type = typeof str;
    let res = 0;
    if (str_type !== 'string' && str_type !== 'number') {
      return NaN
    }

    str = String(str).trim().split('.')[0]
    let length = str.length;
    if (!length) {

      return NaN
    }

    if (!radix) {
      radix = 10;
    }
    if (typeof radix !== 'number' || radix < 2 || radix > 36) {
      return NaN
    }

    for (let i = 0; i < length; i++) {
      let arr = str.split('').reverse().join('');
      res += Math.floor(arr[i]) * Math.pow(radix, i)
    }

    return res;
  }
}